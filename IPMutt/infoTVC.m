//
//  infoTVC.m
//  IPMutt
//
//  Created by Skip Barker on 11/5/13.
//  Copyright (c) 2013 Mutt Studios. All rights reserved.
//
//
//  This file is part of IPMutt.
//
//  IPMutt is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  IPMutt is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with IPMutt.  If not, see <http://www.gnu.org/licenses/>.

#import "infoTVC.h"

@interface infoTVC ()

@end

@implementation infoTVC
@synthesize webSiteCell,twitterCell,adnCell,emailCell,gitHubCell;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    //#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 2;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    //#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 2;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *theCellClicked = [self.tableView cellForRowAtIndexPath:indexPath];
    if (theCellClicked == webSiteCell) {
        //Do stuff
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.muttstudios.com"]];
    }
    if (theCellClicked == twitterCell) {
        //Do stuff
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://twitter.com/muttstudios"]];
    }
    if (theCellClicked == adnCell) {
        //Do stuff
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://alpha.app.net/muttstudios"]];
    }
    if (theCellClicked == gitHubCell) {
        //Do stuff
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://github.com/MuttStudios/IPMutt"]];
    }
    if (theCellClicked == emailCell) {
        
        NSLog(@"Mail Support.");
        
        // Email Subject
        NSString *emailTitle = @"IPMutt 3.0";
        // Email Content
        NSString *messageBody = @"";
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@"support@muttstudios.com"];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
        
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (IBAction)dismissModalView:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
