//
//  mainTVC.h
//  IPMutt
//
//  Created by Skip Barker on 11/6/13.
//  Copyright (c) 2013 Mutt Studios. All rights reserved.
//
//
//  This file is part of IPMutt.
//
//  IPMutt is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  IPMutt is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with IPMutt.  If not, see <http://www.gnu.org/licenses/>.

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface mainTVC : UITableViewController <UIActionSheetDelegate,MFMailComposeViewControllerDelegate> {
// For storing the string response and address
NSMutableString *response;
NSMutableString *ipAddr;
}

@property (nonatomic, strong) IBOutlet UITableViewCell *ipAddressCell;
@property (nonatomic, strong) IBOutlet UITableViewCell *timeRefreshCell;

@end
